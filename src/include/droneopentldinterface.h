#ifndef DRONEOPENTLDINTERFACE_H
#define DRONEOPENTLDINTERFACE_H

#include <string>
#include <ostream>

////// ROS  ///////
#include "ros/ros.h"

// module dependencies
//#include "droneModule.h"

#include "droneMsgsROS/BoundingBox.h"
#include "std_msgs/Float32.h"

// rostopic hz /OpenTLD/tld_fps
#define DRONEOPENTLDINTERFACE_TLD_EXPECTED_FPSCHANNEL_RATE ( 15.0 )
//#define DRONEOPENTLDINTERFACE_TEST_CALLBACK_GET_DATA_CORRUPTION

class DroneOpenTLDInterface
{
private:
    ros::NodeHandle n;
    // OpenTLD related resources
    ros::Subscriber bounding_box_sub;
    ros::Subscriber fps_sub;
    droneMsgsROS::BoundingBox bounding_box;
    std_msgs::Float32 fps;
    void boundingBoxSubCallback(const droneMsgsROS::BoundingBox::ConstPtr &msg);
    void fpsSubCallback(const std_msgs::Float32::ConstPtr &msg);
protected:
    bool reset();

private: // OpenTLD node status
    bool tracking_object;
    bool is_object_on_frame;
    bool received_fps_atleast_once;
    ros::Time last_time_recieved_fps;
    ros::Time last_time_recieved_bb;

public:
    DroneOpenTLDInterface( );
    ~DroneOpenTLDInterface();

private: bool moduleOpened;
public:
    void open(ros::NodeHandle & nIn, std::string ardroneName);
    bool isTrackingObject();
    bool isObjectOnFrame();
    void print();
    bool getBoundingBox( ros::Time &timestamp, int &x, int &y, int &width, int &height, float &confidence, float &current_fps);


};

#endif // DRONEOPENTLDINTERFACE_H
